To verify a credit/debit card or apply for a limit increase, please complete the agreement below:
For each credit/debit card used, enter the account information below and print the agreement.
In the space provided at the bottom of the form, make an imprint of your credit/debit card by placing the card under the printed form and very lightly rubbing a pencil over the top. Applies to embossed cards only.
Sign and date the bottom of the agreement.
Fax the completed agreement along with:

- A legible photocopy of your picture ID (e.g. Drivers License or Passport)
- Photocopies of the front and back of your credit/debit card. Enlarge to 120% for best results.
- A copy of a recent utility bill or official mail that contains your name and address as registered on your customer account.
 Authorization Agreement & Account Verification 
By submitting this signed and dated form, along with the additional information requested, I am authorizing and fully acknowledging the following:
I am the authorized cardholder and will honor all purchases initiated by me to my account whether completed by telephone or Internet
I certify that the information provided and supporting documentation is truthful and accurate
I am of age of majority (18 years or older depending on your jurisdiction)
I have read and accepted the terms of use as listed elsewhere on this website
Please enter your information below.
Site Name:	KontoFX EUR
Customer Account ID:	
Full Name:	
Phone:	
Email:	
Card Number:	
Card Expiration Date:	 / 
Issuing Bank Name:	
Issuing Bank Phone:	 Toll free # on back of card
I hereby understand and authorize the above as evidenced by my signature below.

Signature: _________________________________________    Date: _____________________

To make an imprint of your embossed card: -->
Place card under box outline
Rub pencil very lightly over card
Ensure your name, number & expiration date are clear and readable.
Please complete, scan and EMAIL to: support@kontofx.info
Include a photocopy of your picture ID and copies of the front and back of credit card.
 Skriv ut den här sidan...
 
